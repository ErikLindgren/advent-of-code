#include "Player.h"


#include <algorithm>
#include <fstream>
#include <memory>

bool ReadFile(std::vector<std::string>& output);
int main(int argc, char* argv)
{
   std::unique_ptr<Player> player = std::make_unique<Player>(0, 0, Player::Face::NORTH);
   std::cout << "Player Position: (" << player->getX() << "," << player->getY() << ") Player Direction: " << player->getFacing() << " Player Distance to Target: " << player->getDistanceToTarget() << "\n";

   std::vector<std::string> lines;
   if (ReadFile(lines)) {
      std::cout << "number of lines in file: " << lines.size() << "\n";
   } else std::cout << "Unable to open file\n";

   player->setPathToTarget(lines);

   //player->step(Player::RIGHT, 8);
   //player->step(Player::RIGHT, 4);
   //player->step(Player::RIGHT, 4);
   //player->step(Player::RIGHT, 8);

   std::cout << "Player Position: (" << player->getX() << "," << player->getY() << ") Player Direction: " << player->getFacing() << " Player Distance to Target: " << player->getDistanceToTarget() << "\n";
   system("pause");
   return 0;
}


bool ReadFile(std::vector<std::string>& output)
{
   std::string line;
   std::ifstream myfile("../assets/input.txt");
   if (myfile.is_open())
   {
      while (getline(myfile, line, ','))
      {
         line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
         output.push_back(line);
      }
      myfile.close();
      return true;
   }
   else return false; 
}