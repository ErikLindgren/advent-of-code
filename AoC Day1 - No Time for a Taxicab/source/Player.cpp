
#include "Player.h"

//Public
Player::Player(int x, int y, Player::Face _direction) : pos(x,y), target(0,0),facing(_direction)
{
}

bool Player::setTarget(int x, int y)
{
   target.x = x;
   target.y = y;

   return true;
}
void Player::setPathToTarget(std::vector<std::string>& path) {
   std::vector<DirectionData>* dataPath = new std::vector<DirectionData>();
   for (auto dataPoint : path) {
      if (dataPoint[0] == 'R') {
         dataPoint.erase(0, 1);
         dataPath->push_back(DirectionData(Player::Direction::RIGHT, dataPoint != "" ? std::stoi(dataPoint) : 0));
      }
      else if (dataPoint[0] == 'L') {
         dataPoint.erase(0, 1);
         dataPath->push_back(DirectionData(Player::Direction::LEFT, dataPoint != "" ? std::stoi(dataPoint) : 0));
      }
      else std::cout << "Some weird error with fileparsing in pathToTarget(string) \n";
   }
   setPathToTarget(*dataPath);
}
bool Player::Reset() {
   pos.x = 0;
   pos.y = 0;
   facing = NORTH;
   target.x = 0;
   target.y = 0;
   PathData.clear();
   return true;
}
int Player::getDistanceToTarget() { return abs(pos.x) + abs(pos.y); }
int Player::getX() { return pos.x; }
int Player::getY() { return pos.y; }
Player::Face Player::getFacing() { return facing; }

//Private
void Player::setPathToTarget(const std::vector<DirectionData>& data) { PathData = data; calculateTargetFromPath(); }
bool Player::step(Direction dir, int amount)
{
   // step amount of steps in dir
   int newDir = (int) facing + (int) dir;
   if (newDir == 4) newDir = 0;
   else if (newDir == -1) newDir = 3;

   facing = (Face) newDir;
   for (int i = 0; i < amount; i++)
   {
      switch (facing)
      {
      case Player::NORTH: {pos.y += 1; }break;
      case Player::WEST: {pos.x -= 1; }break;
      case Player::SOUTH: {pos.y -= 1; }break;
      case Player::EAST: {pos.x += 1; }break;
      default: break;
      }
      if (walkedBefore())
         return false;
   }
   return true;
}
void Player::calculateTargetFromPath()
{
   //calculate and return the distande from position to target; abs(stuff)
   for (auto dataPoint : PathData) {
      if (!step(dataPoint.direction, dataPoint.amount))
         break;
   }
}

bool Player::walkedBefore()
{
   for (auto vec : traversedPath) {
      if (vec == pos) return true;
   }
   traversedPath.push_back(pos);
   return false;
}
