#pragma once

#include <vector>
#include <iostream>
#include <string>

class Player
{
public:
   enum Face
   {
      NORTH,
      EAST,
      SOUTH,
      WEST,
   };
   enum Direction
   {
      STRAIGHT = 0,
      RIGHT = 1,
      LEFT = -1,
   };

   struct Vector2
   {
      bool operator ==(const Vector2& v)
      {
         if (x == v.x) {
            if (y == v.y) {
               return true;
            }
         }
         return false;
      }
      bool operator<(const Vector2& v)
      {
         if (x < v.x) {
            if (y < v.y) {
               return true;
            }
         }
         return false;
      }

      Vector2(int _x, int _y) {
         x = _x;
         y = _y;
      }
      int x;
      int y;
   };

   struct DirectionData
   {
      DirectionData(Direction dir, int _amount = 1)
      {
         direction = dir;
         amount = _amount;
      }
      int amount;
      Direction direction;
   };

   Player(int x = 0, int y = 0, Face _direction = NORTH);

   bool setTarget(int x, int y);
   void setPathToTarget(std::vector<std::string>& path);
   bool Reset();
   int getDistanceToTarget();
   int getX();
   int getY();
   Face getFacing();
   bool step(Direction dir, int amount);
private:
   void setPathToTarget(const std::vector<DirectionData>& data);
   void calculateTargetFromPath();
   bool walkedBefore();

   std::vector<DirectionData> PathData;
   std::vector<Vector2> traversedPath;
   Face facing;
   Vector2 pos, target;
};