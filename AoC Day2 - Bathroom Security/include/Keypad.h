//Keypad.h

#pragma once

class Keypad
{
public:
   enum Direction
   {
      UP,
      RIGHT,
      DOWN,
      LEFT,
   };

   struct Node
   {
      Node(char _value = ' ') : value(_value) {}
      bool addNeighbours(Node* nodeU, Node* nodeR, Node* nodeD, Node* nodeL) {
         U = nodeU;
         R = nodeR;
         D = nodeD;
         L = nodeL;
         return true;
      }

      Node *U, *R, *L, *D;
      char value;
   };
   Keypad(Node* startNode);
   bool MoveFinger(Direction dir);
   const char GetFingerPosition();
private:

   Node* activeNode;
};