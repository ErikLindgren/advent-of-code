//Keypad.cpp

#include "Keypad.h"

Keypad::Keypad(Node* startNode) : activeNode(startNode)
{
}

bool Keypad::MoveFinger(Direction dir)
{
   switch (dir)
   {
   case Keypad::UP: {
      if (activeNode->U != nullptr) activeNode = activeNode->U; else return false;
   }break;
   case Keypad::RIGHT: {
      if (activeNode->R != nullptr) activeNode = activeNode->R; else return false;
   }break;
   case Keypad::DOWN: {
      if (activeNode->D != nullptr) activeNode = activeNode->D; else return false;
   }break;
   case Keypad::LEFT: {
      if (activeNode->L != nullptr) activeNode = activeNode->L; else return false;
   }break;
   default:break;
   }
   return true;
}

const char Keypad::GetFingerPosition()
{
   return activeNode->value;
}