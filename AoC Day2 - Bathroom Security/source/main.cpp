#include "Keypad.h"

#include <algorithm>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <vector>

bool ReadFile(std::vector<std::string>& output);
int main(int argc, char* argv)
{

   std::vector<std::string> lines;
   if (ReadFile(lines)) {
      std::cout << "number of lines in file: " << lines.size() << "\n";
   }
   else std::cout << "Unable to open file\n";

   //Create the nodes
   Keypad::Node* node1 = new Keypad::Node('1');
   Keypad::Node* node2 = new Keypad::Node('2');
   Keypad::Node* node3 = new Keypad::Node('3');
   Keypad::Node* node4 = new Keypad::Node('4');
   Keypad::Node* node5 = new Keypad::Node('5');
   Keypad::Node* node6 = new Keypad::Node('6');
   Keypad::Node* node7 = new Keypad::Node('7');
   Keypad::Node* node8 = new Keypad::Node('8');
   Keypad::Node* node9 = new Keypad::Node('9');
   Keypad::Node* nodeA = new Keypad::Node('A');
   Keypad::Node* nodeB = new Keypad::Node('B');
   Keypad::Node* nodeC = new Keypad::Node('C');
   Keypad::Node* nodeD = new Keypad::Node('D');

   node1->addNeighbours(nullptr, nullptr, node3, nullptr);
   node2->addNeighbours(nullptr, node3, node6, nullptr);
   node3->addNeighbours(node1, node4, node7, node2);
   node4->addNeighbours(nullptr, nullptr, node8, node3);
   node5->addNeighbours(nullptr, node6, nullptr, nullptr);
   node6->addNeighbours(node2, node7, nodeA, node5);
   node7->addNeighbours(node3, node8, nodeB, node6);
   node8->addNeighbours(node4, node9, nodeC, node7);
   node9->addNeighbours(nullptr, nullptr, nullptr, node8);
   nodeA->addNeighbours(node6, nodeB, nullptr, nullptr);
   nodeB->addNeighbours(node7, nodeC, nodeD, nodeA);
   nodeC->addNeighbours(node8, nullptr, nullptr, nodeB);
   nodeD->addNeighbours(nodeB, nullptr, nullptr, nullptr);




   std::unique_ptr<Keypad> keypad = std::make_unique<Keypad>(node7);
   std::cout << "Player Start Position: " << keypad->GetFingerPosition() << "\n\nKeyCombo: ";
   for (auto line : lines) {
      for (auto c : line) {
         if (c == 'U') keypad->MoveFinger(Keypad::UP);
         if (c == 'R') keypad->MoveFinger(Keypad::RIGHT);
         if (c == 'D') keypad->MoveFinger(Keypad::DOWN);
         if (c == 'L') keypad->MoveFinger(Keypad::LEFT);
      }
      std::cout << keypad->GetFingerPosition();
   }
   std::cout << "\n\n";

   system("pause");
   return 0;
}


bool ReadFile(std::vector<std::string>& output)
{
   std::string line;
   std::ifstream myfile("../assets/input.txt");
   if (myfile.is_open())
   {
      while (getline(myfile, line))
      {
         //line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
         output.push_back(line);
      }
      myfile.close();
      return true;
   }
   else return false;
}
