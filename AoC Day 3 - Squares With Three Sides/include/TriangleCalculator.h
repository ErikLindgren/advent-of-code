#pragma once

#include <vector>
#include <string>

class TriangleCalculator
{
public:
   
   struct TriangleData
   {
      TriangleData(int _x, int _y ,int _z) : x(_x), y(_y), z(_z)
      {/*could potentially sort for largest and shit*/}

      int x, y, z;
   };

   TriangleCalculator();

   void setTriangles(std::vector<std::string>& Triangles);

   
private:

   std::vector<TriangleData> tris;
};