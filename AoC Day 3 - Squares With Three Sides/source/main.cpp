#include "TriangleCalculator.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>


bool ReadFile(std::vector<std::string>& output);
int main(int argc, char* argv)
{

   std::vector<std::string> lines;
   if (ReadFile(lines)) {
      std::cout << "number of lines in file: " << lines.size() << "\n";
   }
   else std::cout << "Unable to open file\n";

   std::unique_ptr<TriangleCalculator> triangleCalculator = std::make_unique<TriangleCalculator>();
   triangleCalculator->setTriangles(lines);

   system("pause");
   return 0;
}


bool ReadFile(std::vector<std::string>& output)
{
   std::string line;
   std::ifstream myfile("../assets/input.txt");
   if (myfile.is_open())
   {
      while (getline(myfile, line, ' '))
      {
         output.push_back(line);
      }
      myfile.close();
      return true;
   }
   else return false;
}